import React from 'react'
import PropTypes from 'prop-types'
import {RaisedButton} from "material-ui";

const Link = ({active, children, onClick}) => (
  <RaisedButton label={children}
                onClick={onClick}
                disabled={active}
                backgroundColor={'#00ADFF'}
                labelColor={'#ffffff'}
                style={{
                  height: 26,
                  marginLeft: '4px',
                }}
  />
);

Link.propTypes = {
  active: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired
};

export default Link