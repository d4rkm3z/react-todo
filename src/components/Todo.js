import React from 'react'
import PropTypes from 'prop-types'
import Checkbox from 'material-ui/Checkbox';

const Todo = ({onClick, completed, text}) => (
    <Checkbox onClick={onClick} checked={completed} label={text} />
);

Todo.propTypes = {
    onClick: PropTypes.func.isRequired,
    completed: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired
}

export default Todo
