import React from 'react'
import VisibleActions from './VisibleActions'
import AddTodo from '../containers/AddTodo'
import VisibleTodoList from '../containers/VisibleTodoList'
 
const App = () => (
  <div>
    <AddTodo />
    <VisibleActions />
    <VisibleTodoList />
  </div>
);
 
export default App