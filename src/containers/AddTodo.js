import React from 'react'
import {connect} from 'react-redux'
import {addTodo} from '../actions'
import {RaisedButton, TextField} from "material-ui";

const AddTodo = ({dispatch}) => {
  const style = {
    margin: 12,
    height: 26
  };
  let textInput = React.createRef();

  return (
    <div>
      <form
        onSubmit={e => {
          e.preventDefault();
          if (!textInput.current.getValue().trim()) {
            return
          }
          dispatch(addTodo(textInput.current.getValue()));
          textInput.current.getInputNode().value = ''
        }}
      >
        <TextField
          floatingLabelText="Enter task name"
          ref={textInput}
        />
        <RaisedButton label="Add Todo" primary={true} style={style} type="submit"/>
      </form>
    </div>
  )
};

export default connect()(AddTodo)